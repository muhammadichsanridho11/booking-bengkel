package com.bengkel.booking.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class BengkelService {
	private static int bookingIdCounter = 1;
	private static String lastCustomerId = "";

	private static Scanner input = new Scanner(System.in);

	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	
	//Login
	public static void login(List<Customer> listAllCustomers, Scanner input) {
		boolean isLooping = true;
		
		while (isLooping) {
			System.out.println("=== Login ===");
			System.out.print("Masukkan CustomerId: ");
			String customerId = input.nextLine();
			System.out.print("Masukkan Password: ");
			String password = input.nextLine();
			Customer loggedInCustomer = null;
			boolean loginSuccess = false;
		
			for (Customer customer : listAllCustomers) {
				if (customer.getCustomerId().equals(customerId) && customer.getPassword().equals(password)) {
					loggedInCustomer = customer;
					loginSuccess = true;
					isLooping = false;
					MenuService.mainMenu(loggedInCustomer);
					
					break;
				}
			}
		
			if (!loginSuccess) {
				System.out.println("UserId atau Password yang anda masukkan salah, mohon ulangi dengan benar.");
			}
		}
		

		
	}

	
	
	//Info Customer
	public static void getCustomerbyId(){
		
	}
	private static String generateBookingId(Customer loggedInCustomer) {
		String customerId = loggedInCustomer.getCustomerId();
		String customerIdPart = customerId.substring(customerId.length() - 3);
		if (!customerId.equals(lastCustomerId)) {
			lastCustomerId = customerId;
			bookingIdCounter = 1;
		}
		return "Book-Cust-" + String.format("%03d", bookingIdCounter++) + "-" + customerIdPart;
	}


	
	//Booking atau Reservation
	public static void bookingMenu(List<ItemService> listAllItemService, Customer loggedInCustomer,
			Scanner scanner, List<BookingOrder> bookingOrderList) {
			System.out.println("Booking Bengkel");

			System.out.println("Masukan Vehicle Id:");
			String vehicleId = scanner.nextLine();
			System.out.println();
			boolean isVehicleFound = false;
			String vehicleType = null;
			for (Vehicle vehicle : loggedInCustomer.getVehicles()) {
				if (vehicle.getVehiclesId().equals(vehicleId)) {
					isVehicleFound = true;
					vehicleType = vehicle.getVehicleType();
					break;
				}
			}
			PrintService.displayServiceList(listAllItemService, vehicleType);

			BookingOrder bookingOrder = new BookingOrder();
			bookingOrder.setCustomer(loggedInCustomer);
			bookingOrder.setServices(new ArrayList<ItemService>());

			String serviceId;
			double totalHarga = 0;
			boolean isAddingService = true;

			int serviceCount = 0;
			int maxServiceCount = (loggedInCustomer instanceof MemberCustomer) ? loggedInCustomer.getMaxNumberOfService() : 1;
			while (isAddingService) {
				System.out.println("Silahkan masukan Service Id:");
				serviceId = scanner.nextLine();
				if (serviceId.equals("0")) {
					System.out.println("Kembali ke Home Menu.");
					return;
				}
				ItemService selectedService = null;
				for (ItemService service : listAllItemService) {
					if (service.getServiceId().equalsIgnoreCase(serviceId)) {
						selectedService = service;
						break;
					}
				}

				if (selectedService != null) {
					totalHarga += selectedService.getPrice();
					bookingOrder.getServices().add(selectedService);
					bookingOrder.setTotalServicePrice(totalHarga);
					serviceCount++;
					if (serviceCount < maxServiceCount) {
						System.out.println("Apakah anda ingin menambahkan Service Lainnya? (Y/T)");
						String choice = scanner.nextLine();
						if (!choice.equalsIgnoreCase("Y")) {
							isAddingService = false;
						}
					} else {
						System.out.println("Anda telah mencapai batas maksimal layanan yang dapat dipesan.");
						break;
					}
				} else {
					System.out.println("Service tidak ditemukan.");
				}
			}

			

			bookingOrder.setBookingId(generateBookingId(loggedInCustomer));

			String paymentMethod;
			if (loggedInCustomer instanceof MemberCustomer) {
				do {
					System.out.println("\nSilahkan Pilih Metode Pembayaran (Saldo Coin atau Cash)");
					paymentMethod = scanner.nextLine();
					if (!paymentMethod.equalsIgnoreCase("Saldo Coin") && !paymentMethod.equalsIgnoreCase("Cash")) {
						System.out.println("Input tidak valid. Harap masukkan 'Saldo Coin' atau 'Cash'.");
					}
				} while (!paymentMethod.equalsIgnoreCase("Saldo Coin") && !paymentMethod.equalsIgnoreCase("Cash"));
		
				bookingOrder.setPaymentMethod(paymentMethod);
			} else {
				System.out.println("\nMetode Pembayaran: Cash");
				paymentMethod = "Cash";
				bookingOrder.setPaymentMethod(paymentMethod);
			}

			if (paymentMethod.equalsIgnoreCase("Cash")) {
				while (true) {
					System.out.println("\nMasukkan jumlah pembayaran:");
					double paymentAmount = scanner.nextDouble();
					scanner.nextLine();
					if (paymentAmount >= bookingOrder.getTotalServicePrice()) {
						bookingOrder.setTotalPayment(bookingOrder.getTotalServicePrice());
						break;
					} else {
						System.out.println("Pembayaran tidak mencukupi. Silakan masukkan jumlah pembayaran yang mencukupi.");
					}
				}
			} else {
				bookingOrder.calculatePayment();
				if (loggedInCustomer instanceof MemberCustomer) {
					MemberCustomer memberCustomer = (MemberCustomer) loggedInCustomer;
					double saldo = memberCustomer.getSaldoCoin();
					System.out.println(saldo);
					double totalPayment = bookingOrder.getTotalPayment();
					if (saldo >= totalPayment) {
						memberCustomer.setSaldoCoin(saldo - totalPayment);
					} else {
						System.out.println("\nSaldo Coin tidak mencukupi. Silakan pilih metode pembayaran yang lain.");
						return;
					}
				}
			}

			bookingOrderList.add(bookingOrder);
			
			
			System.out.println("\nBooking Berhasil!!!");
			System.out.println("Total Harga Service : " + bookingOrder.getTotalServicePrice());

			System.out.println("Total Pembayaran : " + bookingOrder.getTotalPayment());
	}


	//Top Up Saldo Coin Untuk Member Customer
	public static void topUpMenu(Customer loggedInCustomer, Scanner scanner) {
		if (loggedInCustomer instanceof MemberCustomer) {
			System.out.println("Top Up Saldo Coin\n");
			double topUpAmount = Validation.validasiAngka("Masukan Besaran Top Up: ", "Amount harus berupa angka!","^[0-9]+(\\.[0-9]+)?$");
			MemberCustomer memberCustomer = (MemberCustomer) loggedInCustomer;
			double currentBalance = memberCustomer.getSaldoCoin();
			double newBalance = currentBalance + topUpAmount;
			memberCustomer.setSaldoCoin(newBalance);

			System.out.println("\nTop Up berhasil dilakukan!");
			System.out.println("Saldo Coin saat ini: " + newBalance);
		} else {
			System.out.println("Maaf fitur ini hanya untuk Member saja!");
		}
	}

	//Logout
	
}

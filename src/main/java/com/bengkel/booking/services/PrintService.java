package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {

	
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.
	public static void displayCustomerInfo(Customer loggedInCustomer) {
		System.out.println("CONTOH GAMBARAN Infomasi Customer MENU\n");
		String customerType = (loggedInCustomer instanceof MemberCustomer) ? "Member" : "Non Member";
		System.out.println(customerType);
		System.out.println("Customer Profile");

		System.out.println("+------------------------------+");
		System.out.println("Customer Id : " +  loggedInCustomer.getCustomerId());
		System.out.println("Nama : " + loggedInCustomer.getName());
		System.out.println("Customer Status : " + customerType);
		System.out.println("Alamat : " + loggedInCustomer.getAddress());
		if (loggedInCustomer instanceof MemberCustomer) {
			System.out.println("Saldo Koin" + ((MemberCustomer) loggedInCustomer).getSaldoCoin());
		}
		System.out.println("+------------------------------+");

		List<Vehicle> vehicles = loggedInCustomer.getVehicles();
		if (vehicles != null && !vehicles.isEmpty()) {
			System.out.println("\nList Kendaraan:");
			System.out.println("+--------------------------------------------------------------+");
			System.out.printf("| %-4s | %-10s | %-7s | %-15s | %-10s |\n", "No", "Vehicle Id", "Warna",
					"Tipe Kendaraan", "Tahun", "");
			System.out.println("--------------------------------------------------------------");

			for (int i = 0; i < vehicles.size(); i++) {
				Vehicle vehicle = vehicles.get(i);
				System.out.printf("| %-4d | %-10s | %-7s | %-15s | %-10d |\n", i + 1, vehicle.getVehiclesId(),
						vehicle.getColor(), vehicle.getVehicleType(), vehicle.getYearRelease(), "");
			}
			System.out.println("--------------------------------------------------------------");
		} else {
			System.out.println("\nCustomer ini tidak memiliki kendaraan terdaftar.");
		}
	}

	public static void displayBookingList(List<BookingOrder> bookingOrderList, Customer loggedInCustomer) {
		System.out.println("Daftar Booking:");
		System.out.println(
				"+============================================================================================================================================+");
		System.out.printf("| %-4s | %-20s | %-14s | %-15s | %-13s | %-14s | %-40s|\n",
				"No", "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment","List Service");
		System.out.println(
				"+============================================================================================================================================+");
		int count = 0;
		for (int i = 0; i < bookingOrderList.size(); i++) {
			BookingOrder booking = bookingOrderList.get(i);
			if (booking.getCustomer().equals(loggedInCustomer)) {
				StringBuilder listService = new StringBuilder();
				for (ItemService service : booking.getServices()) {
					listService.append(service.getServiceName()).append(", ");
				}

				if (listService.length() > 0) {
					listService.delete(listService.length() - 2, listService.length());
				}

				System.out.printf("| %-4d | %-20s | %-14s | %-15s | %-13.2f | %-14.2f | %-40s |\n",
						(count + 1), booking.getBookingId(), booking.getCustomer().getName(),
						booking.getPaymentMethod(), booking.getTotalServicePrice(),
						booking.getTotalPayment(),listService.toString());
				System.out.println(
						"+============================================================================================================================================+");
				count++;
			}
		}
		if (count == 0) {
			System.out.println("Tidak ada booking untuk pelanggan ini.");
		}
	}

	public static void displayServiceList(List<ItemService> listAllItemService,
			String vehicleType) {
		System.out.println("\tList Service yang Tersedia:");
		System.out.println(
				"\t+=========================================================================================================================+");
		System.out.printf("\t| %-3s | %-10s | %-15s | %-15s | %-10s |\n", "No", "Service Id", "Nama Service","Tipe Kendaraan", "Harga");
		System.out.println(
				"\t+=========================================================================================================================+");
		for (int i = 0; i < listAllItemService.size(); i++) {
			ItemService service = listAllItemService.get(i);
			if (service.getVehicleType().equals(vehicleType)) {
				System.out.printf("\t| %-3d | %-10s | %-15s | %-15s | %-10.2f |\n",
						(i + 1), service.getServiceId(), service.getServiceName(),
						service.getVehicleType(), service.getPrice());
			}
		}
		System.out.println(
				"\t+=========================================================================================================================+");
		System.out.printf("\t| %-3s | %-59s |\n", "0", "Kembali Ke Home Menu");
		System.out.println(
				"\t+=========================================================================================================================+");
	}

	
}
